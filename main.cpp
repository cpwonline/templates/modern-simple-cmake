#include <iostream>
#include <vector>

#include <Poco/JSON/JSON.h>
#include "Poco/JSON/Object.h"
#include "Poco/JSON/Array.h"
#include <Poco/JSON/Parser.h>
#include <Poco/Dynamic/Var.h>
#include <Poco/Dynamic/Struct.h>


using namespace Poco;

int main()
{
    std::vector<std::string> json_vector
    {
        "{\"element\": 2}"
        ,"{\"element\": \"\"}"
        ,"{\"element\": [1, 2, 3]}"
        ,"{\"element\": null}"
        ,"{\"element\": \"text\"}"
        ,"{\"element\": {\"text\": 2}}"
    };

    for(std::string json : json_vector)
    {
        // Iniciamos el parser
        JSON::Parser parser;

        Dynamic::Var dynamic_manager = parser.parse(json);
        auto json_object = dynamic_manager.extract<JSON::Object::Ptr>();

        if(json_object->isArray("element"))
            std::cout << "\n isArray";
        if(json_object->isObject("element"))
            std::cout << "\n isObject";
        if(json_object->isNull("element"))
            std::cout << "\n isNULL";

        if(!json_object->get("element").isEmpty())
            std::cout << "\n JSON: " << json_object->get("element").toString() << std::endl;
    }

    return 0;
}
